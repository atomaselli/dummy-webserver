package RESTApi;
use Dancer2; 
use Dancer2::Plugin::ParamTypes;


our $VERSION = '0.2';

set serializer => 'JSON';



get '/' => sub {

push_header( 'X-Server-ID' => $ENV{'HOSTNAME'} );

return { 
        'client-ip'     => request->address,
        'client-agent'  => request->user_agent,
        'forwarded-for' => request->forwarded_for_address,
        'keep-alive'    => request->keep_alive,
       }
};

true;
#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin/../extlib/lib/perl5";



# use this block if you don't need middleware, and only have a single target Dancer app to run here
use RESTApi;

RESTApi->to_app;

=begin comment
# use this block if you want to include middleware such as Plack::Middleware::Deflater

use RESTApi;
use Plack::Builder;

builder {
    enable 'Deflater';
    RESTApi->to_app;
}

=end comment

=cut

=begin comment
# use this block if you want to mount several applications on different path

use RESTApi;
use RESTApi_admin;

use Plack::Builder;

builder {
    mount '/'      => RESTApi->to_app;
    mount '/admin'      => RESTApi_admin->to_app;
}

=end comment

=cut

